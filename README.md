# 3 Musketeers Image for Google Container Registry

[![pipeline status](https://gitlab.com/fixl/docker-musketeers-gcr/badges/master/pipeline.svg)](https://gitlab.com/fixl/docker-musketeers-gcr/-/pipelines)
[![version](https://fixl.gitlab.io/docker-musketeers-gcr/version.svg)](https://gitlab.com/fixl/docker-musketeers-gcr/-/commits/master)
[![size](https://fixl.gitlab.io/docker-musketeers-gcr/size.svg)](https://gitlab.com/fixl/docker-musketeers-gcr/-/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/fixl/musketeers-gcr)](https://hub.docker.com/r/fixl/musketeers-gcr)
[![Docker Stars](https://img.shields.io/docker/stars/fixl/musketeers-gcr)](https://hub.docker.com/r/fixl/musketeers-gcr)

Lightweight image with essential tools for a [3 Musketeers](https://3musketeers.io/) project. In
addition, this image is shipped with [Docker Credential helper for GCR](https://github.com/GoogleCloudPlatform/docker-credential-gcr)
so it can be used more easily with GCR on Google Cloud.

> Based on flemay's [docker image](https://github.com/flemay/docker-images/tree/master/docker-musketeers)

## Tools

- Docker
  - useful when wanting to run docker commands inside a container
- Compose
- make
- zip
- curl
- git
- openssl
- bash
- envsubst
- jq
- docker-credential-gcr
  - for when you want to push to Google Container Registry

## Build the image

```bash
make build
```

## Inspect the image

```bash
docker inspect --format='{{ range $k, $v := .Config.Labels }}{{ printf "%s=%s\n" $k $v}}{{ end }}' fixl/musketeers-gcr:latest
```

## Update regions

To update regions, run the following script against a project your have access to:

```
gcloud artifacts locations list > regions
```
