FROM docker

ARG CREDENTIAL_HELPER_VERSION
ARG REGIONS

RUN apk --no-cache add --upgrade \
      make \
      zip \
      git \
      curl \
      docker-compose \
      openssl \
      bash \
      gettext \
      jq \
      yq \
    && curl -fsSL "https://github.com/GoogleCloudPlatform/docker-credential-gcr/releases/download/v${CREDENTIAL_HELPER_VERSION}/docker-credential-gcr_linux_amd64-${CREDENTIAL_HELPER_VERSION}.tar.gz" \
      | tar xz --to-stdout docker-credential-gcr \
      > /usr/bin/docker-credential-gcr && chmod +x /usr/bin/docker-credential-gcr \
    && docker-credential-gcr configure-docker \
    && for region in $(echo ${REGIONS}); do docker-credential-gcr configure-docker --registries=${region}-docker.pkg.dev; done

CMD ["make"]
